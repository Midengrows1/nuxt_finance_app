import Vue from "vue"
import Vuex from "vuex"
import api from "../api/routes.js"

Vue.use(Vuex)

const state = () => ({
  routes: {
    register: {
      api: 'auth/signup'
    },
    login: {
      api: "auth/login"
    }
  },
  user: {},
})
const getters = {
  user: state => state.user
}
const mutations = {

}
const actions = {
  // CHECK_IT({
  //   commit
  // }) {

  //   if (!localStorage.token) {
  //     this.$router.push('Login')
  //   }

  // },
  REGISTER({
    commit
  }) {

    let obj = {}
    let fm = new FormData(event.target)
    fm.forEach((value, key) => {
      obj[key] = value
    })
    console.log(obj);

    api.post({
        api: state().routes.register.api,
        obj
      })
      .then(res => {
        if (res.status == 200 || res.status == 201 || res.status == 202) {
          localStorage.user = JSON.stringify(res.data)
          localStorage.token = res.data.token
          this.$cookies.set('token', res.data.token, {
            path: 'Mainpage',
            maxAge: 60 * 60 * 24 * 7
          })
          this.$router.push('Mainpage')
        }
      })
      .catch(err => {
        alert('error')
      })
  },
  LOGIN_IT({
    commit
  }) {
    let obj = {}
    let fm = new FormData(event.target)
    fm.forEach((value, key) => {
      obj[key] = value
    })
    console.log(obj);

    api.post({
        api: state().routes.login.api,
        obj
      })
      .then(res => {
        if (res.status == 200 || res.status == 201 || res.status == 202) {
          localStorage.user = JSON.stringify(res.data)
          localStorage.token = res.data.token
          this.$cookies.set('token', res.data.token, {
            path: 'Mainpage',
            maxAge: 60 * 60 * 24 * 7
          })
          this.$router.push('Mainpage')

        }
      })
      .catch(err => {
        alert('error')
      })
  },
  SIGN_OUT({
    commit
  }) {
    localStorage.clear()
    this.$router.push('Registration')
  }
}
export default {
  state,
  getters,
  mutations,
  actions,
}
