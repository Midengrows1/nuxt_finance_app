import Vuex from 'vuex'
import Vue from 'vue'
import users from './users.js'

Vue.use(Vuex)

export default () => new Vuex.Store({
  state: () => ({
    
  }),
  mutations: {},
  actions: {},
  modules: {
    users
  }
})

